# Neovim Configuration Switcher

This project provides an easy way to manage and switch between multiple Neovim configurations. It includes a script (`sm-nvim-switcher`) and supports various installation methods depending on your Linux distribution.

## Features
- Seamlessly switch between Neovim configurations like:
  - AstroNvim
  - LazyVim
  - LunarVim
  - NvChad
- Automated setup for NvChad if not already configured.
- Compatible with multiple Linux distributions including Solus.

## Installation

1. Clone the repository:
   ```bash
   git clone https://gitlab.com/shastenm/nvim-configs.git
   cd nvim-config/
   ```

2. Run the `install.sh` script:
   ```bash
   ./install.sh
   ```

   This will:
   - Move the `sm-nvim-switcher` script to `~/.local/bin`.

## Setup Options (for Solus and Other Distros)

### Option A: Append to `~/.bashrc`
Add the following lines to your `~/.bashrc` file:
```bash
# Neovim configuration
export PATH="$HOME/.local/bin:$PATH"
```
Run the following command to apply changes:
```bash
source ~/.bashrc
```

### Option B: Move to `~/.bash` Directory
Move the Neovim configuration to the `~/.bash` directory:
```bash
mv neovim ~/.bash
```

### Option C: Move to `/usr/share/defaults/etc/profile.d/`
Move the Neovim configuration to the system-wide profile directory:
```bash
sudo mv neovim /usr/share/defaults/etc/profile.d/
```

## Using the `sm-nvim-switcher` Script

The `sm-nvim-switcher` script is used to switch between Neovim configurations. 

### Usage
```bash
sm-nvim-switcher <config_name>
```
- Replace `<config_name>` with one of the following:
  - `astro` (for AstroNvim)
  - `lazy` (for LazyVim)
  - `lunar` (for LunarVim)
  - `nvchad` (for NvChad)

### Example
```bash
sm-nvim-switcher nvchad
```
This will:
- Automatically set up NvChad if it is not already configured.
- Create a symbolic link to the selected configuration in `~/.config/nvim`.

### Script Description
The `sm-nvim-switcher` script:
1. Checks if a configuration name is provided as an argument.
2. Sets up NvChad if selected and not already configured.
3. Ensures the desired configuration exists in `~/.config/nvim-configs/`.
4. Updates the symbolic link at `~/.config/nvim` to point to the selected configuration.

## TODO
- [ ] - video on how to use this
- [ ] - test this on various linux distros
- [ ] - test this with zsh
- [ ] - test this with fish shell
- [ ] - add other nvim distros
- [ ] - add my own configuration
- [x] - get nvchad to work

## Contributing
Contributions are welcome! Feel free to submit issues or pull requests to improve the script or documentation.

## License
This project is licensed under the MIT License.

