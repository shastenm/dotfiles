-- Open Up My TODO
local opts = { noremap = true, silent = true }
vim.api.nvim_set_keymap("n", "<leader>td", [[<cmd>edit ~/Documents/second-brain/_Daily/TODO.md<CR>]], opts)

local opts = { noremap = true, silent = true }
vim.api.nvim_set_keymap("n", "<leader>tn", [[<cmd>edit ~/Documents/second-brain/_Temporary/notes.md<CR>]], opts)

vim.keymap.set("n", "<leader>zo", function()
	return require("fold-cycle").open()
end, { silent = true, desc = "Fold-cycle: open folds" })
vim.keymap.set("n", "<leader>zp", function()
	return require("fold-cycle").close()
end, { silent = true, desc = "Fold-cycle: close folds" })
vim.keymap.set("n", "<leader>zc", function()
	return require("fold-cycle").close_all()
end, { remap = true, silent = true, desc = "Fold-cycle: close all folds" })

-- English Spelling
vim.api.nvim_set_keymap("n", "<leader>se", ":setlocal spell spelllang=en_us<CR>", { noremap = true, silent = true })

-- Spanish Spelling
vim.api.nvim_set_keymap("n", "<leader>sp", ":setlocal spell spelllang=es_mx<CR>", { noremap = true, silent = true })

-- English and Spanish Spelling
vim.api.nvim_set_keymap(
	"n",
	"<leader>sb",
	":setlocal spell spelllang=en_us,es_mx<CR>",
	{ noremap = true, silent = true }
)

-- Autocomplete
vim.api.nvim_set_keymap("i", "<C-Space>", "v:lua.vim.lsp.buf.completion()", { expr = true, noremap = true })
