--global variables
vim.cmd([[
set ai
set encoding=utf-8
set expandtab
set number relativenumber
set noswapfile
set scrolloff=5
set tabstop=4
set softtabstop=4
set shiftwidth=2
set shiftround
]])
vim.api.nvim_exec(
	[[
  augroup DisableFolding
    autocmd!
    autocmd BufWritePost,BufEnter * set nofoldenable foldmethod=manual foldlevelstart=99
  augroup END
]],
	false
)

--Other settings
vim.opt.backspace = "2"
vim.opt.showcmd = true
vim.opt.laststatus = 2
vim.opt.autowrite = true
vim.opt.cursorline = true
vim.opt.autoread = true
vim.opt.clipboard = "unnamedplus"
vim.opt.conceallevel = 2 -- or 1 if you prefer

vim.keymap.set("n", "<leader>h", ":nohlsearch<CR>")

vim.api.nvim_set_keymap("i", "jj", "<Esc>", { noremap = true, silent = true })

-- Save file
vim.keymap.set("n", "<leader>w", "<cmd>w<cr>", { noremap = true, desc = "Save window" })

-- Set python3 host prog
vim.g.python3_host_prog = "/usr/bin/python3"

-- Disable Perl, Ruby
vim.g.loaded_perl_provider = 0
vim.g.loaded_ruby_provider = 0

-- Set Spell Directory
vim.opt.spellfile = { "~/.local/share/nvim/site/spell/en.utf-8.add", "~/.local/share/nvim/site/spell/es.utf-8.add" }

local package_path = "/home/cyborg/.luarocks/share/lua/5.1/?.lua;"
	.. "/home/cyborg/.luarocks/share/lua/5.1/?/init.lua;"
	.. "/usr/local/share/lua/5.1/?.lua;"
	.. "/usr/local/share/lua/5.1/?/init.lua;"
	.. "/usr/share/lua/5.1/?.lua;"
	.. "/usr/share/lua/5.1/?/init.lua;"
local cpath = "/home/cyborg/.luarocks/lib/lua/5.1/?.so;" .. "/usr/local/lib/lua/5.1/?.so;" .. "/usr/lib64/lua/5.1/?.so;"
package.path = package.path .. ";" .. package_path
package.cpath = package.cpath .. ";" .. cpath
