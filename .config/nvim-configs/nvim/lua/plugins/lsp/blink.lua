return {
	"saghen/blink.cmp",
	lazy = false,
	dependencies = {
		"rafamadriz/friendly-snippets", -- Predefined snippets
		"L3MON4D3/LuaSnip", -- Snippet engine
	},
	version = "v0.*",
	opts = {
		keymap = {
			-- set to 'none' to disable the 'default' preset
			preset = "none",

			["<Up>"] = { "select_prev", "fallback" },
			["<Down>"] = { "select_next", "fallback" },
			["<C-k>"] = { "select_prev", "fallback" },
			["<C-j>"] = { "select_next", "fallback" },

			-- disable a keymap from the preset
			["<C-e>"] = { "hide", "fallback" },
			["<S-Return>"] = { "select_and_accept" },
			["<Tab>"] = { "snippet_forward", "fallback" },
			["<S-Tab>"] = { "snippet_backward", "fallback" },

			-- show with a list of providers
			["<C-space>"] = {
				function(cmp)
					cmp.show({ providers = { "snippets" } })
				end,
			},

			-- control whether the next command will be run when using a function
			["<C-n>"] = {
				function(cmp)
					if some_condition then
						return
					end -- runs the next command
					return true -- doesn't run the next command
				end,
				"select_next",
			},

			-- optionally, separate cmdline keymaps
			-- cmdline = {}
		},
	},
}
