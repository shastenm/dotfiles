return {
	"neovim/nvim-lspconfig",
	config = function()
		local lspconfig = require("lspconfig")

		-- Python
		lspconfig.pyright.setup({})

		-- JavaScript/TypeScript
		lspconfig.ts_ls.setup({})

		-- Bash (sh)
		lspconfig.bashls.setup({})

		-- Docker
		lspconfig.dockerls.setup({})

		-- Go (Golang)
		lspconfig.gopls.setup({})

		-- Haskell
		lspconfig.hls.setup({})

		-- C language
		lspconfig.clangd.setup({})

		-- JSON
		lspconfig.jsonls.setup({})

		-- TOML
		lspconfig.taplo.setup({})

		-- YAML
		lspconfig.yamlls.setup({})
	end,
}
