---@type LazySpec
return {
  "nvim-treesitter/nvim-treesitter",
  opts = {
    ensure_installed = {
      "bash",
      "javascript",
      "lua",
      "markdown",
      "markdown_inline",
      "python",
      "vim",
      -- add more arguments for adding more treesitter parsers
    },
    highlight = {
      enable = true,
    },
    incremental_selection = {
      enable = true,
    },
    textobjects = {
      enable = true,
    },
  },
}
