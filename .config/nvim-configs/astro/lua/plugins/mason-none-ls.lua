return {
  "jay-babu/mason-null-ls.nvim",
  event = { "BufReadPre", "BufNewFile" },
  dependencies = {
    "williamboman/mason.nvim",
    "nvimtools/none-ls.nvim",
  },
  config = function()
    require("mason").setup()
    require("mason-null-ls").setup {
      ensure_installed = { "ruff" },
      automatic_installation = false,
      handlers = {
        -- Set up Ruff as both a formatter and a linter
        ruff = function()
          local null_ls = require "null-ls"
          null_ls.register {
            null_ls.builtins.formatting.ruff,
            null_ls.builtins.diagnostics.ruff,
          }
        end,
      },
    }
  end,
}
