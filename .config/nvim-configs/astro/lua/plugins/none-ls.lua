---@type LazySpec
return {
  "nvimtools/none-ls.nvim",
  opts = function(_, opts)
    local null_ls = require "null-ls"

    local sources = {
      null_ls.builtins.formatting.stylua,
      null_ls.builtins.formatting.prettier.with { filetypes = { "json", "yaml" } },
      null_ls.builtins.formatting.shfmt.with { args = { "-i", "4" } },
    }

    -- Only insert new sources, do not replace existing ones
    opts.sources = require("astrocore").list_insert_unique(opts.sources, sources)

    local augroup = vim.api.nvim_create_augroup("LspFormatting", {})

    null_ls.setup {
      sources = sources,
      on_attach = function(client, bufnr)
        if client.supports_method "textDocument/formatting" then
          vim.api.nvim_clear_autocmds { group = augroup, buffer = bufnr }
          vim.api.nvim_create_autocmd("BufWritePre", {
            group = augroup,
            buffer = bufnr,
            callback = function() vim.lsp.buf.format { async = false } end,
          })
        end
      end,
    }
  end,
}
