#!/bin/bash

# Set error handling
set -euo pipefail

# Function to log messages with timestamps
log_message() {
	echo "[$(date '+%Y-%m-%d %H:%M:%S')] $1"
}

# Function to handle errors
handle_error() {
	log_message "ERROR: $1"
	exit 1
}

# Clone the repository (replace 'URL' with the actual repo URL)
log_message "Cloning repository..."
git clone https://gitlab.com/shastenm/nvim-configs.git || handle_error "Failed to clone repository"
#git clone git@gitlab.com:shastenm/nvim-configs.git  || handle_error "Failed to clone repository"

 Move 'nvim-configs' to the '~/.config' directory
if [ -d nvim-configs ]; then
  log_message "Moving nvim-configs to ~/.config/"
  mv nvim-configs ~/.config/ || handle_error "Failed to move nvim-configs to ~/.config"
else
  handle_error "nvim-configs directory not found"
fi

# Change directory to the newly moved 'nvim-configs'
cd ~/.config/nvim-configs || handle_error "Failed to navigate to nvim-configs directory"

# Function to display the menu and handle user choice
append_or_move_neovim() {
	if [ ! -f neovim ]; then
		handle_error "neovim file not found in the current directory"
	fi

echo '[ -f ~/.config/nvim-configs/neovim ] && source ~/.config/nvim-configs/neovim' >> ~/.bashrc

# Backup existing Neovim configurations
log_message "Backing up existing Neovim configurations..."
for dir in ~/.config/nvim ~/.local/share/nvim ~/.local/state/nvim ~/.cache/nvim; do
	if [ -d "$dir" ]; then
		mv "$dir" "${dir}.bak-$(date '+%Y%m%d')" || handle_error "Failed to backup $dir"
		log_message "Backed up $dir to ${dir}.bak-$(date '+%Y%m%d')"
	else
		log_message "Directory $dir doesn't exist, skipping backup"
	fi
done

# Call the function to handle the neovim configuration
append_or_move_neovim

log_message "Setup completed successfully!"
echo "
✨ Ready to switch Neovim distros painlessly! ✨
Everything has been set up successfully.
Enjoy your new Neovim configuration!
"
