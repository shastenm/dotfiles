#!/bin/bash

# Background processes
clipmenud &
nitrogen --restore &
picom -b &
sxhkd -c ~/.config/bspwm/sxhkdrc &
# tint2 &
xfsettings &
xset -dpms &
/usr/bin/spice-vdagent &
/usr/libexec/xfce-polkit &
/usr/bin/VBoxClient-all &
/bin/sh ~/.screenlayout/screen.sh &
#dzen2
#sleep 1                                          # Adjust delay as needed to ensure stalonetray is fully started
#dzen2 -dock -ta l -fn 'ubuntu-mono' -x 1920 -p & # Launch dzen2 with your preferred settings
