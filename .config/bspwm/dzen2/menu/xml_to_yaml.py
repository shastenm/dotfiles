import xml.etree.ElementTree as ET
import yaml

def xml_to_dict(element):
    if list(element):
        return {
            element.tag: {
                'attributes': element.attrib,
                'children': [xml_to_dict(child) for child in element]
            }
        }
    else:
        return {
            element.tag: {
                'attributes': element.attrib,
                'text': element.text.strip() if element.text else None
            }
        }

def parse_openbox_menu(xml_file):
    tree = ET.parse(xml_file)
    root = tree.getroot()
    return xml_to_dict(root)

def dict_to_yaml(data, yaml_file):
    with open(yaml_file, 'w') as f:
        yaml.dump(data, f, default_flow_style=False)

xml_file = 'menu.xml'
yaml_file = 'menu.yaml'

data_dict = parse_openbox_menu(xml_file)
dict_to_yaml(data_dict, yaml_file)

print(f'Converted {xml_file} to {yaml_file}')

