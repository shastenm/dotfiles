-- Filename: ~/github/dotfiles-latest/wezterm/wezterm.lua

-- Pull in the wezterm API
local wezterm = require("wezterm")

-- This will hold the configuration.
local config = wezterm.config_builder()

-- Tell WezTerm where to find custom color schemes
config.color_scheme_dirs = { os.getenv("HOME") .. "/.config/wezterm/colors" }
config.color_scheme = "solarized-osaka-night"

-- Other config options
config.term = "xterm-kitty"
config.enable_kitty_graphics = true
config.font = wezterm.font("JetBrains Mono")
config.font_size = 15
config.enable_tab_bar = false
config.window_close_confirmation = "NeverPrompt"
config.cursor_blink_ease_out = "Constant"
config.cursor_blink_ease_in = "Constant"
config.cursor_blink_rate = 400

config.window_padding = {
	left = 2,
	right = 2,
	top = 15,
	bottom = 0,
}

return config
