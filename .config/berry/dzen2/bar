#!/bin/bash

# Source the color definitions
source ~/.config/berry/dzen2/colors

# Define icon paths
ICON_PATH="$HOME/.config/berry/dzen2/icons"

# ICONS
ARCH_ICON="^i($ICON_PATH/arch.xbm)"
LAYOUT_ICON="^i($ICON_PATH/filesystem.xbm)"
PDF_ICON="^i($ICON_PATH/file.xbm)"
CALENDAR_ICON="^i($ICON_PATH/calendar.xbm)"
MEMORY_ICON="^i($ICON_PATH/memory.xbm)"
CPU_ICON="^i($ICON_PATH/processor.xbm)"

# Scripts
DOCS="sh ~/.config/berry/scripts/dzen/docs"
DOTS="sh ~/.config/berry/scripts/dzen/dots"
PKEYS="sh ~/.config/berry/scripts/toggle-berry-keys"
SKEYS="sh ~/.config/berry/scripts/toggle-sxhkd-keys"
MENU="rdmenu"

# Chinese characters array (using non-nerd font characters)
#NUMERALS=("I" "II" "III" "IV")
NUMERALS=("1" "2" "3" "4" "5" "6" "7" "8" "9")
#NUMERALS=("终" "网" "开" "系" "文" "图" "音" "影" "编")

# Function to get the content for the left section
get_left_content() {
  # Use global variable for color
  menu_item="^ca(1,$MENU)$ARCH_ICON^ca()"
  layout_item="^ca(1,sm-layout)$LAYOUT_ICON^ca()"
  clip_item="^ca(1,clipmenu)$PDF_ICON^ca()"
  doc_item="^ca(1,$DOCS)$PDF_ICON^ca()"
  dot_item="^ca(1,$DOTS)$PDF_ICON^ca()"

  # Add virtual desktops
  desktops=$(wmctrl -d | awk '{print $1}')
  active_desktop=$(wmctrl -d | grep '*' | awk '{print $1}')
  occupied_desktops=$(wmctrl -l | awk '{print $2}' | sort -u)

  desktop_content=""
  for desktop in $desktops; do
    character=${NUMERALS[$desktop]}
    if [[ "$desktop" == "$active_desktop" ]]; then
      desktop_content+="^ca(1,wmctrl -s $desktop)${C2}${character}^ca() "
    elif [[ "$occupied_desktops" =~ "$desktop" ]]; then
      desktop_content+="^ca(1,wmctrl -s $desktop)${C10}${character}^ca() "
    else
      desktop_content+="^ca(1,wmctrl -s $desktop)${FG}${character}^ca() "
    fi
  done

  # Add xtitle at the end with color $C2
  xtitle=$(xtitle)
  xtitle_item="${C2}${xtitle}"

  # Concatenate desktop content and icons
  left_content="${C2}${menu_item}        $desktop_content        ${C10}${doc_item} ${C12}${dot_item} ${C14}${clip_item}       ${C2}$xtitle_item"

  echo "$left_content"
}

# Function to get the content for the center section
get_center_content() {
  pkeys="^ca(1,$PKEYS)$PDF_ICON^ca()"
  skeys="^ca(1,$SKEYS)$PDF_ICON^ca()"

  center_content="${C12}${pkeys} ${C14}${skeys}"
  echo "$center_content"
}

# Function to get the content for the right section
get_right_content() {
  mem_info=$(free -h | awk 'NR==2 {print $3 "/" $2}')
  disk_info=$(df -h / | awk 'NR==2 {print $3 " / " $2}')
  datetime=$(date "+%Y-%m-%d %H:%M:%S")
  content_length=$(echo -n "${CALENDAR_ICON} ${datetime}" | wc -c)
  right_padding=$((1620 - $content_length * 6 - 50))

  calendar_item="^ca(1,gsimplecal)${CALENDAR_ICON}^ca()"
  memory_item="${MEMORY_ICON} ${mem_info}"
  disk_item="${CPU_ICON} ${disk_info}"
  right_content="${C16}${disk_item}    ${C14}${memory_item}    ${C12}${calendar_item} ${C12}${datetime}"

  echo "^pa($right_padding)$right_content"
}

# Function to update the panel content
update_panel() {
  left_content=$(get_left_content)
  center_content=$(get_center_content)
  right_content=$(get_right_content)

  # Center the center content
  center_padding=$((($PANEL_WIDTH - ${#center_content} * 1) / 2))

  # Padding for left content
  left_padding=0

  echo "^pa($left_padding)$left_content ^pa($center_padding)$center_content $right_content"
}

# Set margin and width
SCREEN_WIDTH=1920
PANEL_WIDTH=1620
MARGIN=150
PANEL_X=$MARGIN  # Start panel 150px from the left edge

# Initial update of the panel
update_panel

# Launch dzen2 with adjusted geometry and other parameters
while true; do
  update_panel
  sleep 1
done | dzen2 -fn 'ubuntu-mono' -x $PANEL_X -y 0 -w $PANEL_WIDTH -h 26 -bg '#151515' -fg '#ffffff' -p &

