#!/bin/bash

# Get the window ID of the currently focused window
WIN_ID=$(xdotool getactivewindow)

# Check if the window ID was found
if [ -n "$WIN_ID" ]; then
  # Get screen dimensions
  SCREEN_WIDTH=$(xrandr | grep '*' | awk '{print $1}' | cut -d 'x' -f 1)
  SCREEN_HEIGHT=$(xrandr | grep '*' | awk '{print $1}' | cut -d 'x' -f 2)

  # Define the desired dimensions
  WIDTH=720
  HEIGHT=720

  # Calculate the position to center the window
  X=$(((SCREEN_WIDTH - WIDTH) / 2))
  Y=$(((SCREEN_HEIGHT - HEIGHT) / 2))

  # Resize and move the window to the center
  wmctrl -ir $WIN_ID -e 0,$X,$Y,$WIDTH,$HEIGHT
else
  echo "No active window found"
fi
