#!/bin/bash

# Get the window ID of the currently focused window
WIN_ID=$(xdotool getactivewindow)

# Check if the window ID was found
if [ -n "$WIN_ID" ]; then
  # Get screen dimensions
  SCREEN_WIDTH=$(xrandr | grep '*' | awk '{print $1}' | cut -d 'x' -f 1)
  SCREEN_HEIGHT=$(xrandr | grep '*' | awk '{print $1}' | cut -d 'x' -f 2)

  # Define the desired margins
  MARGIN_TOP=45 # Adding 5px extra padding on top
  MARGIN_BOTTOM=35
  MARGIN_SIDES=10
  MOVE_UP=18

  # Calculate new dimensions and position
  WIDTH=$(((SCREEN_WIDTH - 2 * MARGIN_SIDES) / 2))
  HEIGHT=$(((SCREEN_HEIGHT - MARGIN_TOP - MARGIN_BOTTOM) / 2))
  X=$((SCREEN_WIDTH / 2 + MARGIN_SIDES / 2))
  Y=$((SCREEN_HEIGHT / 2 + MARGIN_TOP / 2 - MOVE_UP))

  # Resize and move the window to the lower right half
  wmctrl -ir $WIN_ID -e 0,$X,$Y,$WIDTH,$HEIGHT
else
  echo "No active window found"
fi
