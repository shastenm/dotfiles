#!/bin/bash

# Get the window ID of the currently focused window
WIN_ID=$(xdotool getactivewindow)

# Check if the window ID was found
if [ -n "$WIN_ID" ]; then
  # Get screen dimensions
  SCREEN_WIDTH=$(xrandr | grep '*' | awk '{print $1}' | cut -d 'x' -f 1)
  SCREEN_HEIGHT=$(xrandr | grep '*' | awk '{print $1}' | cut -d 'x' -f 2)

  # Define the desired margins
  MARGIN_TOP=65
  MARGIN_BOTTOM=5
  MARGIN_SIDES=15

  # Calculate new dimensions and position
  WIDTH=$((SCREEN_WIDTH - 2 * MARGIN_SIDES))
  HEIGHT=$((SCREEN_HEIGHT - MARGIN_TOP - MARGIN_BOTTOM))
  X=$MARGIN_SIDES
  Y=$MARGIN_TOP

  # Center position calculations
  CENTER_X=$(((SCREEN_WIDTH - WIDTH) / 2))
  CENTER_Y=$(((SCREEN_HEIGHT - HEIGHT) / 2))

  # Resize and move the window
  wmctrl -ir $WIN_ID -e 0,$CENTER_X,$CENTER_Y,$WIDTH,$HEIGHT
else
  echo "No active window found"
fi
