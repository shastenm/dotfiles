#!/bin/bash

# Get the window ID of the currently focused window
WIN_ID=$(xdotool getactivewindow)

# Check if the window ID was found
if [ -n "$WIN_ID" ]; then
  # Get screen dimensions
  SCREEN_WIDTH=$(xrandr | grep '*' | awk '{print $1}' | cut -d 'x' -f 1)
  SCREEN_HEIGHT=$(xrandr | grep '*' | awk '{print $1}' | cut -d 'x' -f 2)

  # Define the desired margins
  MARGIN_TOP=33 # Adding 5px extra padding on top
  MARGIN_BOTTOM=55
  MARGIN_SIDES=10

  # Calculate new dimensions and position
  WIDTH=$(((SCREEN_WIDTH - 2 * MARGIN_SIDES) / 2))
  HEIGHT=$(((SCREEN_HEIGHT - MARGIN_TOP - MARGIN_BOTTOM) / 2))
  X=$((SCREEN_WIDTH / 2 + MARGIN_SIDES / 2))
  Y=$((MARGIN_TOP - 5)) # Move up by 5px

  # Resize and move the window to the upper right half
  wmctrl -ir $WIN_ID -e 0,$X,$Y,$WIDTH,$HEIGHT
else
  echo "No active window found"
fi
