conky.config = {
    alignment = 'top_right',
    gap_x = 30,
    gap_y = 70,
    font = 'DejaVu Sans Mono:size=6',
    default_color = 'white',
    own_window = true,
    own_window_type = 'desktop',
    own_window_transparent = false,
    own_window_argb_visual = true,
    own_window_argb_value = 190, -- Transparency level (0-255)
    double_buffer = true,
    use_xft = true,
    update_interval = 1.0,
    minimum_width = 320, -- Width of the text + 2 * padding
    maximum_width = 420, -- Width of the text + 2 * padding
    draw_shades = false,
};
${alignc}${font DejaVu Sans Mono:bold:size=18}${color #c0caf5} SPECTRWM KEYS ${font}${color}

${font DejaVu Sans Mono:bold:size=10}${color #8ec07c} Terminal ${font}${color}
      
      MOD+Return: Open terminal (kitty)
      MOD+Shift+Return: Open menu (dmenu_run)

${font DejaVu Sans Mono:bold:size=10}${color #d08770} Window Management ${font}${color}

      MOD+space: Cycle layout
      MOD+Shift+backslash: Flip layout
      MOD+t: Toggle floating
      MOD+grave: Focus free
      MOD+m: Focus main
      MOD+j/Tab: Focus next
      MOD+k/Shift+Tab: Focus previous
      MOD+Shift+a: Focus prior
      MOD+u: Focus urgent
      MOD+Shift+grave: Toggle free
      MOD+Shift+e: Toggle fullscreen
      MOD+Shift+equal: Grow height
      MOD+Shift+minus: Shrink height
      MOD+w: Iconify
      MOD+Shift+i: Initialize screen
      MOD+comma: Add to master
      MOD+period: Remove from master
      MOD+l: Grow master
      MOD+h: Shrink master
      MOD+e: Maximize toggle
      MOD+Shift+bracketright: Move down
      MOD+bracketleft: Move left
      MOD+bracketright: Move right
      MOD+Shift+bracketleft: Move up

${font DejaVu Sans Mono:bold:size=10}${color #b8bb26} Workspaces ${font}${color}

      MOD+Shift+{1-9,0}: Move to workspace {1-10}
      MOD+{1-9,0}: Go to workspace {1-10}
      MOD+Right: Go to next workspace
      MOD+Up: Go to next workspace (all)
      MOD+Shift+Up: Move to next workspace
      MOD+Left: Go to previous workspace
      MOD+Down: Go to previous workspace (all)
      MOD+Shift+Down: Move to previous workspace
      MOD+a: Go to prior workspace

${font DejaVu Sans Mono:bold:size=10}${color #d08770} Marking and Tagging ${font}${color}
      
      MOD+z: Toggle marked
      MOD+a: Attach marked
      MOD+t: Toggle tagged

${font DejaVu Sans Mono:bold:size=10}${color #8ec07c} Control and Menus ${font}${color}

      MOD+Shift+R: Reload Spectrwm
      MOD+D: Show command dialog
      MOD+V: Show search dialog
      MOD+R: Show root menu
      MOD+W: Show window menu
      MOD+C: Show goto client menu
      MOD+Shift+I: Show icon menu
      MOD+Shift+w: Uniconify
      MOD+Shift+v: Version info

${font DejaVu Sans Mono:bold:size=10}${color #fb4934} Window Actions ${font}${color}

      MOD+Shift+x: Kill window
      MOD+x: Delete window
      MOD+equal: Grow width
      MOD+minus: Shrink width
      MOD+Shift+space: Reset stack
      MOD+Shift+{period,comma}: Adjust stack
      MOD+Shift+s: Swap main

${font DejaVu Sans Mono:bold:size=10}${color #689d6a} Miscellaneous ${font}${color}

      MOD+Shift+d: Dump windows
      MOD+d: Debug toggle
      MOD+s: Screenshot all
      MOD+Shift+s: Screenshot window
      MOD+Shift+Right: Swap to next
      MOD+Shift+k: Swap previous

