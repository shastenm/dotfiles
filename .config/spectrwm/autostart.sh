#!/bin/bash

# Background processes
clipmenud &
nitrogen --restore &
picom -b &
sxhkd -c ~/.config/spectrwm/sxhkdrc &
# tint2 &
xfsettings &
xset -dpms &
#pkill dzen2 &
conky -c ~/.config/spectrwm/conky/spectr-keys.conf &
/usr/bin/spice-vdagent &
/usr/libexec/xfce-polkit &
/usr/bin/VBoxClient-all &
/bin/sh ~/.screenlayout/screen.sh &
