#!/bin/sh

# Function to get CPU usage
get_cpu_usage() {
    top -bn1 | grep "Cpu(s)" | sed "s/.*, *\([0-9.]*\)%* id.*/\1/" | awk '{print 100 - $1"%"}'
}
# Function to get the center content
get_center_content() {
    ~/.config/spectrwm/chinese_chars
}
# Function to get the right content
get_right_content() {
    cpu_usage=$(get_cpu_usage)
    mem_info=$(free -h | awk 'NR==2 {print $3 "/" $2}')
    disk_info=$(df -h / | awk 'NR==2 {print $3 " / " $2}')
    datetime=$(date "+%a %b %d [%R]")
    echo "+@fg=5;  核 $cpu_usage    +@fg=4; 碟 $disk_info   +@fg=2; 忆 $mem_info     +@fg=1; 历 $datetime"
}
asd

# Main loop
while :; do
    center_content=$(get_center_content)
    right_content=$(get_right_content)
    
    echo "+|C$center_content +|R$right_content"
    sleep 3
done
