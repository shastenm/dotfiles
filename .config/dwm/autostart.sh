
#!/bin/bash

# Background processes
clipmenud &
nitrogen --restore &
picom -b &
sxhkd -c ~/.config/dwm/scripts/sxhkd/sxhkdrc &
tint2 &
xfsettings &
xset -dpms
killall dzen2 &

