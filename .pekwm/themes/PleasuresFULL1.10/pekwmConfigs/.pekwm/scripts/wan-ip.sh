#!/bin/sh
##   from https://ubuntuforums.org/showthread.php?t=413156
##   by tturrisi; April 19th, 2007 at 04:43 PM
#wget -q -O - checkip.dyndns.org|sed -e 's/.*Current IP Address: //' -e 's/<.*$//'
wget -q -O - checkip.dyndns.org | sed -e 's/[^[:digit:]\|.]//g'
