import yaml
import os

def read_menu_yaml(file_path):
    with open(file_path, 'r') as file:
        menu_data = yaml.safe_load(file)
    return menu_data

def extract_submenu_data(menu_data):
    submenu_data = []
    if 'openbox_menu' in menu_data and 'children' in menu_data['openbox_menu']:
        for child in menu_data['openbox_menu']['children']:
            if 'menu' in child:
                menu_attributes = child['menu'].get('attributes', {})
                menu_id = menu_attributes.get('id', '')
                menu_label = menu_attributes.get('label', '')
                
                for submenu in child['menu'].get('children', []):
                    if 'menu' in submenu:
                        submenu_attributes = submenu['menu'].get('attributes', {})
                        submenu_label = submenu_attributes.get('label', '')
                        sublabels = []
                        
                        for subitem in submenu['menu'].get('children', []):
                            if 'item' in subitem:
                                subitem_attributes = subitem['item'].get('attributes', {})
                                sublabel = subitem_attributes.get('label', '')
                                sublabels.append({'Sublabel': sublabel})
                        
                        if sublabels:  # Only add if there are sublabels
                            submenu_data.append({
                                'Submenu Label': f"{submenu_label}",
                                'Submenu ID': f"{submenu_attributes.get('id', '')}",
                                'Sublabels': sublabels
                            })
    
    return submenu_data

if __name__ == "__main__":
    home_dir = os.path.expanduser("~")
    source_file_path = os.path.join(home_dir, '.config', 'bspwm', 'panel', 'scripts', 'menu.yaml')
    destination_file_path = os.path.join(home_dir, '.config', 'bspwm', 'panel', 'scripts', 'submenu.yaml')
    
    print(f"Reading menu.yaml from: {source_file_path}")
    menu_data = read_menu_yaml(source_file_path)
    
    if menu_data:
        submenu_data = extract_submenu_data(menu_data)
        
        if submenu_data:
            print(f"Extracted Submenu Data:")
            with open(destination_file_path, 'w') as file:
                for submenu in submenu_data:
                    file.write(f"- Submenu Label: {submenu['Submenu Label']}\n")
                    file.write(f"  Submenu ID: '{submenu['Submenu ID']}'\n")
                    file.write(f"  - Sublabels:\n")
                    for sublabel in submenu['Sublabels']:
                        file.write(f"    - Sublabel: {sublabel['Sublabel']}\n")
                    file.write("\n")
                    
            print(f"Written submenu data to: {destination_file_path}")
        else:
            print("No submenu data found to write.")
    else:
        print(f"Failed to load or parse YAML file at '{source_file_path}'.")

