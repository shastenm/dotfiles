#!/bin/bash

yaml_file="$HOME/.config/bspwm/panel/menu/submenu.yaml" # Replace with your actual path

# Check if YAML file exists
if [ ! -f "$yaml_file" ]; then
  echo "Error: YAML file not found at $yaml_file"
  exit 1
fi

# Read submenu.yaml and create arrays for Submenu Labels
mapfile -t submenu_labels < <(grep 'Submenu Label: ' "$yaml_file" | sed 's/^.*: //' | grep -v -E 'Science|Utilities|OpenBox')

# Display Submenu Labels using rofi and capture user selection
selected_submenu_label=$(printf '%s\n' "${submenu_labels[@]}" | rofi -dmenu -l 10 -p "Select a Category:" -theme-str 'window {width: 30%;} listview {lines: 10;} entry {placeholder: "Select a Category";}')
[[ -z "$selected_submenu_label" ]] && exit 0

# Find the Sublabels corresponding to the selected Submenu Label
start_line=$(grep -n "Submenu Label: $selected_submenu_label" "$yaml_file" | cut -d: -f1)
if [[ -z "$start_line" ]]; then
  echo "Error: Selected submenu label not found in YAML file"
  exit 1
fi

end_line=$(tail -n +$((start_line + 1)) "$yaml_file" | grep -n "Submenu Label:" | head -n 1 | cut -d: -f1)

if [[ -z "$end_line" ]]; then
  end_line=$(wc -l <"$yaml_file")
else
  end_line=$((start_line + end_line - 1))
fi

mapfile -t sublabels < <(sed -n "${start_line},${end_line}p" "$yaml_file" | grep 'Sublabel: ' | sed 's/^.*: //')

# Remove "AA Fire" (with any leading/trailing whitespace) from sublabels
sublabels=("${sublabels[@]/[[:space:]]*AA Fire[[:space:]]*/}")

# Display the Sublabels using rofi and capture user selection
selected_sublabel=$(printf '%s\n' "${sublabels[@]}" | rofi -dmenu -l 10 -p "Select an Application:" -theme-str 'window {width: 30%;} listview {lines: 10;} entry {placeholder: "Select an Application";}')
[[ -z "$selected_sublabel" ]] && exit 0

# Exception Handling
# Map specific sublabels to commands
case "$selected_sublabel" in
"Document Viewer")
  command="evince"
  ;;
"File Manager PCManFM")
  command="pcmanfm"
  ;;
"File Manager Thunar")
  command="thunar"
  ;;
"GNU Image Manipulation Program")
  command="gimp"
  ;;
"the gimp")
  command="gimp"
  ;;
"obs-studio-plus (25.0.8)")
  command="obs-studio-plus-25.0.8-x86-64_4d338ddfd2565993f1fbcf32bed582b3.AppImage"
  ;;
"GraphicsMagick")
  command="gm display logo"
  ;;
"Google Chrome")
  command="google-chrome-stable"
  ;;
"Xfce Terminal")
  command="xfce4-terminal"
  ;;
# Add more custom mappings here if needed
*)
  # General case: convert to lowercase
  command=$(echo "$selected_sublabel" | tr '[:upper:]' '[:lower:]')
  ;;
esac

# Execute the command
if command -v "${command%% *}" &>/dev/null; then
  eval "$command"
else
  echo "Error: Command '$command' not found"
  exit 1
fi
