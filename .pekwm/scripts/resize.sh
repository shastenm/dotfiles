#!/bin/bash

# Get the window ID of the currently focused window
WIN_ID=$(xdotool getactivewindow)
echo "Window ID: $WIN_ID"

# Check if the window ID was found
if [ -n "$WIN_ID" ]; then
  # Get the raw output from xwininfo for the active window
  XWININFO_OUTPUT=$(xwininfo -id $WIN_ID)
  echo "Raw output from xwininfo:"
  echo "$XWININFO_OUTPUT"

  # Extract screen dimensions and position
  SCREEN_WIDTH=$(echo "$XWININFO_OUTPUT" | grep "Width:" | awk '{print $2}')
  SCREEN_HEIGHT=$(echo "$XWININFO_OUTPUT" | grep "Height:" | awk '{print $2}')
  SCREEN_X=$(echo "$XWININFO_OUTPUT" | grep "Absolute upper-left X:" | awk '{print $4}')
  SCREEN_Y=$(echo "$XWININFO_OUTPUT" | grep "Absolute upper-left Y:" | awk '{print $4}')

  if [ -z "$SCREEN_WIDTH" ] || [ -z "$SCREEN_HEIGHT" ] || [ -z "$SCREEN_X" ] || [ -z "$SCREEN_Y" ]; then
    echo "Failed to parse screen info. Exiting."
    exit 1
  fi

  echo "Active screen dimensions: ${SCREEN_WIDTH}x${SCREEN_HEIGHT} at (${SCREEN_X}, ${SCREEN_Y})"

  # Set exact dimensions and padding
  MARGIN_TOP=28
  RESIZE_WIDTH=1920
  RESIZE_HEIGHT=1052

  # Calculate position for the window
  # Here we're positioning at X=0 since we want it to start from the left edge
  X=$SCREEN_X
  Y=$((SCREEN_Y + MARGIN_TOP))

  echo "Setting dimensions: ${RESIZE_WIDTH}x${RESIZE_HEIGHT} at (${X}, ${Y})"

  # Resize and move the window using wmctrl
  wmctrl -ir $WIN_ID -e 0,$X,$Y,$RESIZE_WIDTH,$RESIZE_HEIGHT
else
  echo "No active window found"
fi
