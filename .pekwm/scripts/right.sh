#!/bin/bash

# Get the window ID of the currently focused window
WIN_ID=$(xdotool getactivewindow)
echo "Window ID: $WIN_ID"

# Check if the window ID was found
if [ -n "$WIN_ID" ]; then
  # Get the raw output from xwininfo for the active window
  XWININFO_OUTPUT=$(xwininfo -id $WIN_ID)
  echo "Raw output from xwininfo:"
  echo "$XWININFO_OUTPUT"

  # Extract screen dimensions and position
  SCREEN_WIDTH=$(echo "$XWININFO_OUTPUT" | grep "Width:" | awk '{print $2}')
  SCREEN_HEIGHT=$(echo "$XWININFO_OUTPUT" | grep "Height:" | awk '{print $2}')
  SCREEN_X=$(echo "$XWININFO_OUTPUT" | grep "Absolute upper-left X:" | awk '{print $4}')
  SCREEN_Y=$(echo "$XWININFO_OUTPUT" | grep "Absolute upper-left Y:" | awk '{print $4}')

  if [ -z "$SCREEN_WIDTH" ] || [ -z "$SCREEN_HEIGHT" ] || [ -z "$SCREEN_X" ] || [ -z "$SCREEN_Y" ]; then
    echo "Failed to parse screen info. Exiting."
    exit 1
  fi

  echo "Active screen dimensions: ${SCREEN_WIDTH}x${SCREEN_HEIGHT} at (${SCREEN_X}, ${SCREEN_Y})"

  # Define the desired margins
  MARGIN_TOP=28
  MARGIN_BOTTOM=10
  MARGIN_SIDES=10
  EXTRA_RIGHT_PADDING=5

  # Calculate new dimensions and position
  WIDTH=$((SCREEN_WIDTH / 2 - MARGIN_SIDES - EXTRA_RIGHT_PADDING))
  HEIGHT=$((SCREEN_HEIGHT - MARGIN_TOP - MARGIN_BOTTOM))
  X=$((SCREEN_X + SCREEN_WIDTH / 2 + MARGIN_SIDES))
  Y=$((SCREEN_Y + MARGIN_TOP))

  echo "Calculated dimensions: ${WIDTH}x${HEIGHT} at (${X}, ${Y})"

  # Resize and move the window
  wmctrl -ir $WIN_ID -e 0,$X,$Y,$WIDTH,$HEIGHT
else
  echo "No active window found"
fi
