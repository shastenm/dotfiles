#!/bin/bash

# Get the window ID of the currently focused window
WIN_ID=$(xdotool getactivewindow)

# Check if the window ID was found
if [ -n "$WIN_ID" ]; then
  # Get the raw output from xwininfo
  XWININFO_OUTPUT=$(xwininfo -id $WIN_ID)

  # Extract screen dimensions and position
  SCREEN_WIDTH=$(echo "$XWININFO_OUTPUT" | grep "Width:" | awk '{print $2}')
  SCREEN_HEIGHT=$(echo "$XWININFO_OUTPUT" | grep "Height:" | awk '{print $2}')
  SCREEN_X=$(echo "$XWININFO_OUTPUT" | grep "Absolute upper-left X:" | awk '{print $4}')
  SCREEN_Y=$(echo "$XWININFO_OUTPUT" | grep "Absolute upper-left Y:" | awk '{print $4}')

  if [ -z "$SCREEN_WIDTH" ] || [ -z "$SCREEN_HEIGHT" ] || [ -z "$SCREEN_X" ] || [ -z "$SCREEN_Y" ]; then
    exit 1
  fi


  # Define the desired dimensions
  WIDTH=720
  HEIGHT=720

  # Calculate the position to center the window on the correct screen
  X=$((SCREEN_X + (SCREEN_WIDTH - WIDTH) / 2))
  Y=$((SCREEN_Y + (SCREEN_HEIGHT - HEIGHT) / 2))


  # Resize and move the window to the center of the correct screen
  wmctrl -ir $WIN_ID -e 0,$X,$Y,$WIDTH,$HEIGHT
fi
