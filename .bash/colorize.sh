#!/bin/bash

export LESS='-R'
export LESS_TERMCAP_mb=$'\E[01;31m'    # Bold red
export LESS_TERMCAP_md=$'\E[01;34m'    # Bold blue
export LESS_TERMCAP_me=$'\E[0m'        # Reset
export LESS_TERMCAP_se=$'\E[0m'        # Reset
export LESS_TERMCAP_so=$'\E[01;44;33m' # Yellow on blue
export LESS_TERMCAP_ue=$'\E[0m'        # Reset
export LESS_TERMCAP_us=$'\E[01;32m'    # Bold green
