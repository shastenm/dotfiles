#!bin/bash

### EXPORT

export TERM="xterm-256color"            # getting proper colors
export HISTCONTROL=ignoredups:erasedups # no duplicate entries
export EDITOR="nvim"

### "bat" as manpager
export MANPAGER="sh -c 'col -bx | bat -l man -p'"

# If not running interactively, don't do anything
[[ $- != *i* ]] && return
unset LD_PRELOAD

### PROMPT
# This is commented out if using starship prompt
#PS1='[\u@\h \W]\$ '
#export PS1="\[$(tput bold)\]\[$(tput setaf 4)\][\[$(tput setaf 4)\]\u\[$(tput setaf 5)\]@\[$(tput setaf 4)\]\h \[$(tput setaf 2)\]\W\[$(tput setaf 4)\]]\[$(tput setaf 2)\]\\$ \[$(tput sgr0)\]"

[ -f ~/.bash/path.sh ] && source ~/.bash/path.sh
[ -f ~/.bash/shopt.sh ] && source ~/.bash/shopt.sh
[ -f ~/.bash/nav.sh ] && source ~/.bash/nav.sh
[ -f ~/.bash/ext.sh ] && source ~/.bash/ext.sh
[ -f ~/.bash/debian.sh ] && source ~/.bash/debian.sh
[ -f ~/.bash/shortcuts.sh ] && source ~/.bash/shortcuts.sh
#[ -f ~/.bash/colorize.sh ] && source ~/.bash/colorize.sh
[ -f ~/.bash/fzf.sh ] && source ~/.bash/fzf.sh
[ -f ~/.bash/git.sh ] && source ~/.bash/git.sh
[ -f ~/.config/nvim-configs/neovim ] && source ~/.config/nvim-configs/neovim
[ -f ~/.fzf.bash ] && source ~/.fzf.bash

#Use neovim for vim if present
command -v nvim >/dev/null && alias vim="nvim" vimdiff="nvim -d"

vf() { fzf | xargs -r -I % $EDITOR %; }

#export PATH="$HOME/.yarn/bin:$HOME/.config/yarn/global/node_modules/.bin:$PATH"

. "$HOME/.cargo/env"

# ~/.bashrc

eval "$(starship init bash)"
